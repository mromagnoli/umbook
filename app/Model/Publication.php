<?php
App::uses('AppModel', 'Model');

class Publication extends AppModel {

	public $belongsTo = array(
		'User' => array(
			'foreignKey' => 'user_id',
		),
		'Wall' => array(
			'foreignKey' => 'wall_id'
		)
	);

}
