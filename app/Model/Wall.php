<?php
App::uses('AppModel', 'Model');

class Wall extends AppModel {

	public $recursive = 2;

	public $hasOne = array(
		'User'
	);

	public $hasMany = array(
		'Publication' => array(
			'contain' => 'User'
		)
	);

}
