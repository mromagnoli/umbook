<?php
if (empty($wall)) {
	return;
}

echo $this->Html->css('publication');

$wallId = $wall['Wall']['id'];
$userId = AuthComponent::user('id');
$publications = $wall['Publication'];

?>
<div class='new-publication'>
	<?=$this->Form->create('Publication', array(
		'url' => array('controller' => 'publications', 'action' => 'add')
	))?>
	<?=$this->Form->textarea('content', array(
		'placeholder' => 'Write here ;)'
	))?>
	<?=$this->Form->hidden('user_id', array('value' => $userId))?>
	<?=$this->Form->hidden('wall_id', array('value' => $wallId))?>
	<?=$this->Form->submit('Publish')?>
</div>
<div class='publications-list'>
	<?php foreach ($publications as $p) : ?>
		<?=$this->element('Publication/publication', array(
			'publication' => $p
		))?>
	<?php endforeach ?>
</div>