<?php
if (empty($user)) {
	return;
}

?>
<div class="menu">
	<div class='menu__user-logged'>
		 <span>Logged as:<span><span class="bold">
			<?=$this->Html->link(
				$user['name'],
				array('controller' => 'walls', 'action' => 'view')
			)?>
		</span>
	</div>
	<div class='menu__user-logout'>
		<span class="bold">
			<?=$this->Html->link(
				'Logout',
				array('controller' => 'users', 'action' => 'logout')
			)?>
		</span>
	</div>
</div>