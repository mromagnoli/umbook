<?php
if (empty($publication['content'])) {
	return;
}

$publisher = !empty($publication['User']['name']) ? $publication['User']['name'] : $publication['User']['username'];
if (empty($publisher)) {
	return;
}

$publisherWallLink = $this->Html->link(
	$publisher,
	array(
		'controller' => 'walls',
		'action' => 'view',
		$publication['User']['wall_id']
	)

);
?>


<div class="publication">
	<span class="publication__title bold"><?=$publisherWallLink?> wrote:</span>
	<p class="publication__body">
		<?=$publication['content']?>
	</p>
	<span class="publication__date"><?=$publication['created']?></span>
</div>