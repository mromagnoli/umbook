<?php
App::uses('AppController', 'Controller');
/**
 * Walls Controller
 *
 * @property Wall $Wall
 * @property PaginatorComponent $Paginator
 */
class WallsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	/*public function index() {
	}*/

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (empty($id)) {
			$id = AuthComponent::user('Wall.id');
		}
		if (!$this->Wall->exists($id)) {
			throw new NotFoundException(__('Invalid wall'));
		}
		$options = array('conditions' => array('Wall.' . $this->Wall->primaryKey => $id));

		$this->set('wall', $this->Wall->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	/*public function add($userId) {
			$this->Wall->create();
			if ($this->Wall->save($this->request->data)) {
				$this->Session->setFlash(__('The wall has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The wall could not be saved. Please, try again.'));
			}
		}
	}*/

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Wall->exists($id)) {
			throw new NotFoundException(__('Invalid wall'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Wall->save($this->request->data)) {
				$this->Session->setFlash(__('The wall has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The wall could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Wall.' . $this->Wall->primaryKey => $id));
			$this->request->data = $this->Wall->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function delete($id = null) {
		$this->Wall->id = $id;
		if (!$this->Wall->exists()) {
			throw new NotFoundException(__('Invalid wall'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Wall->delete()) {
			$this->Session->setFlash(__('The wall has been deleted.'));
		} else {
			$this->Session->setFlash(__('The wall could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}*/
}
